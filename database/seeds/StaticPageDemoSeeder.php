<?php

use Illuminate\Database\Seeder;

class StaticPageDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('static_pages')) {

        	$static_pages = json_decode(json_encode(['about', 'privacy', 'terms', 'contact', 'help', 'faq']));

        	foreach ($static_pages as $key => $value) {

    			$page_details = DB::table('static_pages')->where('type' ,$value)->count();

    			if(!$page_details) {

    				DB::table('static_pages')->insert([
    	         		[
    				        'unique_id' => $value,
                            'title' => $value,
    				        'description' => $value,
    				        'type' => $value,
                            'section_type' => array_rand([STATIC_PAGE_SECTION_1, STATIC_PAGE_SECTION_2, STATIC_PAGE_SECTION_3, STATIC_PAGE_SECTION_4]),
    				        'created_at' => date('Y-m-d H:i:s'),
    				        'updated_at' => date('Y-m-d H:i:s')
    				    ],
				    ]);

    			}

        	}

		}
    }
}
